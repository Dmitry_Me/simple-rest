package com.rest.solutions;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.servlet.GuiceServletContextListener;
import com.google.inject.servlet.ServletModule;
import com.rest.solutions.models.UserProfile;
import com.rest.solutions.models.Bill;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.api.core.ResourceConfig;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;


public class EntryPoint extends GuiceServletContextListener {

    @Override
    protected Injector getInjector() {
        final ResourceConfig resourceConfig = new PackagesResourceConfig("com.rest.solutions");

        return Guice.createInjector(new ServletModule() {
            @Override
            protected void configureServlets() {
                Configuration configuration = new Configuration();
                configuration.addAnnotatedClass(Bill.class);
                configuration.addAnnotatedClass(UserProfile.class);

                Configuration configure = configuration.configure("hibernate.cfg.xml");
                SessionFactory sessionFactory = configure.buildSessionFactory(new StandardServiceRegistryBuilder()
                        .applySettings(configuration.getProperties()).build());

                Session currentSession = sessionFactory.getCurrentSession();
                bind(Session.class).toInstance(currentSession);

                for (Class<?> resource : resourceConfig.getClasses()) {
                    bind(resource);
                }

                serve("/rest/*").with(GuiceContainer.class);
            }
        });
    }
}