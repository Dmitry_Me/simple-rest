package com.rest.solutions.dao;


import com.rest.solutions.models.UserProfile;

public interface UserDao {

    void createUser(UserProfile user);

    UserProfile getUser(String id);

    void updateUser(UserProfile user);

    void deleteUser(UserProfile user);
}
