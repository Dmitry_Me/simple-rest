package com.rest.solutions.dao;

import com.google.inject.Inject;
import com.rest.solutions.models.Bill;
import org.hibernate.Session;

public class BillDaoImpl implements BillDao {

    @Inject
    private Session session;

    public Bill getBillByUserId(String id) {
        Session session = this.session.getSessionFactory().openSession();

        session.beginTransaction();
        try {
            return (Bill) session.get(Bill.class, id);
        } catch (Exception e) {
            session.getTransaction().rollback();
            throw new RuntimeException(e);
        } finally {
            session.getTransaction().commit();
            session.close();
        }
    }

    public void create(Bill bill) {
        Session session = this.session.getSessionFactory().openSession();

        session.beginTransaction();
        try {
            session.save(bill);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
        } finally {
            session.close();
        }
    }

    public void update(Bill bill) {
        Session session = this.session.getSessionFactory().openSession();

        session.beginTransaction();
        try {
            session.update(bill);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
        } finally {
            session.close();
        }
    }

    public boolean transferMoney(Bill source, Bill destination, Double moneyAmount) {
        Session session = this.session.getSessionFactory().openSession();
        session.beginTransaction();

        try {
            if (source.getMoney() - moneyAmount < 0) {
                return false;
            }

            source.setMoney(source.getMoney() - moneyAmount);
            destination.setMoney(destination.getMoney() + moneyAmount);

            session.update(source);
            session.update(destination);

            session.getTransaction().commit();
        } catch (Exception e) {
          session.getTransaction().rollback();
        } finally {
            session.close();
        }

        return true;
    }

}
