package com.rest.solutions.dao;


import com.google.inject.Inject;
import com.rest.solutions.models.UserProfile;
import org.hibernate.Session;

public class UserDaoImpl implements UserDao {

    @Inject
    private Session session;

    public void createUser(UserProfile user) {
        Session session = this.session.getSessionFactory().openSession();

        session.beginTransaction();
        try {
            session.save(user);
        } finally {
            session.getTransaction().commit();
            session.close();
        }
    }

    public UserProfile getUser(String id) {
        Session session = this.session.getSessionFactory().openSession();

        session.beginTransaction();
        try {
            return (UserProfile) session.get(UserProfile.class, id);
        } finally {
            session.getTransaction().commit();
            session.close();
        }
    }

    public void updateUser(UserProfile user) {
        Session session = this.session.getSessionFactory().openSession();

        session.beginTransaction();
        try {
            session.update(user);
        } finally {
            session.getTransaction().commit();
            session.close();
        }
    }

    public void deleteUser(UserProfile user) {
        Session session = this.session.getSessionFactory().openSession();

        session.beginTransaction();
        try {
            session.delete(user);
        } finally {
            session.getTransaction().commit();
            session.close();
        }
    }

}
