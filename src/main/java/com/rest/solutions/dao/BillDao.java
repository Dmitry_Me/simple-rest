package com.rest.solutions.dao;


import com.rest.solutions.models.Bill;

public interface BillDao {

    Bill getBillByUserId(String id);

    void create(Bill bill);

    void update(Bill bill);

    boolean transferMoney(Bill source, Bill destination, Double moneyAmount);
}
