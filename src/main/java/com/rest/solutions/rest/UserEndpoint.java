package com.rest.solutions.rest;


import com.google.inject.Inject;
import com.rest.solutions.models.UserProfile;
import com.rest.solutions.dao.UserDaoImpl;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("user")
public class UserEndpoint {

    @Inject
    private UserDaoImpl userDaoImpl;

    @GET
    @Produces("application/json")
    public Response getUserInfo(@QueryParam("id") String id) {
        UserProfile user = userDaoImpl.getUser(id);
        if (user == null) {
            return Response.status(404).entity("User not found").build();
        }

        return Response.ok(MediaType.APPLICATION_JSON_TYPE).entity(user).build();
    }

    @POST
    @Produces("application/json")
    public Response updateUserProfile(@QueryParam("id") String id,
                                      @QueryParam("name") String name,
                                      @QueryParam("surname") String surname) {
        UserProfile user = userDaoImpl.getUser(id);
        if (user == null) {
            return Response.status(404).build();
        }
        user.setName(name);
        user.setSurname(surname);

        userDaoImpl.updateUser(user);

        return Response.ok(MediaType.APPLICATION_JSON_TYPE).entity(user).build();
    }

    @DELETE
    @Produces("application/json")
    public Response deleteUser(@QueryParam("id") String id) {
        UserProfile user = userDaoImpl.getUser(id);
        if (user == null) {
            return Response.status(404).build();
        }
        userDaoImpl.deleteUser(user);

        return Response.ok(MediaType.APPLICATION_JSON_TYPE).entity(user).build();
    }

    @PUT
    @Produces("application/json")
    public Response createUser(@QueryParam("id") String id,
                               @QueryParam("name") String name,
                               @QueryParam("surname") String surname) {
        UserProfile user = new UserProfile();
        user.setId(id);
        user.setName(name);
        user.setSurname(surname);

        userDaoImpl.createUser(user);

        return Response.ok(MediaType.APPLICATION_JSON_TYPE).build();
    }
}
