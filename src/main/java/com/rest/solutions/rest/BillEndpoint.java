package com.rest.solutions.rest;


import com.google.inject.Inject;
import com.rest.solutions.dao.BillDaoImpl;
import com.rest.solutions.models.Bill;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("bill")
public class BillEndpoint {

    @Inject
    BillDaoImpl billDaoImpl;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getInfo(@QueryParam("id") String id) {
        Bill billByUserId = billDaoImpl.getBillByUserId(id);
        if (billByUserId == null) {
            return Response.status(404).build();
        }
        return Response.ok().entity(billByUserId).build();
    }

    @GET
    @Path("cache")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCache(@QueryParam("id") String id, @QueryParam("cacheAmount") Double cacheAmount) {
        Bill bill = billDaoImpl.getBillByUserId(id);
        if (bill == null) {
            return Response.status(404).entity("Bill not found").build();
        }


        return Response.ok().entity(bill).build();
    }

    @POST
    @Path("cache")
    @Produces(MediaType.APPLICATION_JSON)
    public Response putCache(@QueryParam("id") String id, @QueryParam("cacheAmount") Double cacheAmount) {
        Bill bill = billDaoImpl.getBillByUserId(id);
        if (bill == null) {
            return Response.status(404).entity("Bill not found").build();
        }
        bill.setMoney(bill.getMoney() + cacheAmount);
        billDaoImpl.update(bill);

        return Response.ok().entity("Cache at bill is: " + bill.getMoney()).build();
    }

    @PUT
    @Path("cache")
    @Produces(MediaType.APPLICATION_JSON)
    public Response createBill(@QueryParam("id") String id, @QueryParam("cacheAmount") Double cacheAmount) {
        Bill bill = billDaoImpl.getBillByUserId(id);
        if (bill != null) {
            return Response.status(403).entity("Bill for particular user already exist").build();
        }

        bill = new Bill();
        bill.setUserId(id);
        bill.setMoney(cacheAmount);
        billDaoImpl.create(bill);

        return Response.ok().entity("Bill was created").build();
    }

    @POST
    @Path("transfer")
    @Produces(MediaType.APPLICATION_JSON)
    public Response transferMoneyToUser(@QueryParam("sourceId") String sourceUserId,
                                        @QueryParam("destId") String destinationUserId,
                                        @QueryParam("cacheAmount") Double moneyAmount) {
        if (moneyAmount == null) {
            return Response.status(400).entity("cacheAmount not specified").build();
        }
        Bill sourceBill = billDaoImpl.getBillByUserId(sourceUserId);
        Bill destBill = billDaoImpl.getBillByUserId(destinationUserId);
        if (sourceBill == null) {
            return Response.status(404).entity("Source bill not found").build();
        }
        if (destBill == null) {
            return Response.status(404).entity("Destination bill not found").build();
        }

        if (!billDaoImpl.transferMoney(sourceBill, destBill, moneyAmount)) {
            return Response.status(403).entity("Not enough money on source account").build();
        }

        return Response.ok().entity("Transfered successfully").build();
    }
}
