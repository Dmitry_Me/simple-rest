package com.rest.solutions.rest;


import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import javax.ws.rs.core.MediaType;
import java.io.IOException;

import static junit.framework.Assert.assertEquals;

public class BillEndpointTest extends AbstractRestEndpointTest {

    private String cacheEndpoint = "revolut/bill/cache";

    @Test
    public void testBillDoesntExist() throws IOException {
        Client client = Client.create(new DefaultClientConfig());
        WebResource service = client.resource(getBaseURI());

        ClientResponse resp = service.path(cacheEndpoint)
                .queryParam("id", "111")
                .accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);

        assertEquals(404, resp.getStatus());
    }

    @Test
    public void testCreateBillCheckExists() throws IOException {
        Client client = Client.create(new DefaultClientConfig());
        WebResource service = client.resource(getBaseURI());

        ClientResponse resp = service.path(cacheEndpoint)
                .queryParam("id", "12").queryParam("cacheAmount", "14.2")
                .accept(MediaType.APPLICATION_JSON).put(ClientResponse.class);

        assertEquals(200, resp.getStatus());

        resp = service.path(cacheEndpoint)
                .queryParam("id", "12")
                .accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);

        assertEquals(200, resp.getStatus());
    }

    @Test
    public void testTransferMoneyNegative() throws IOException {
        Client client = Client.create(new DefaultClientConfig());
        WebResource service = client.resource(getBaseURI());

        ClientResponse resp = service.path("revolut/bill/transfer")
                .queryParam("sourceId", "12").queryParam("destId", "13").queryParam("cacheAmount", "14.2")
                .accept(MediaType.APPLICATION_JSON).post(ClientResponse.class);

        assertEquals(404, resp.getStatus());
    }

    @Test
    public void testTransferMoneyPositive() throws IOException {
        Client client = Client.create(new DefaultClientConfig());
        WebResource service = client.resource(getBaseURI());

        // creating users
        String baseUserPath = "revolut/user";
        String sourceUserId = "15";
        ClientResponse resp = service.path(baseUserPath)
                .queryParam("id", sourceUserId).queryParam("name", "Tom").queryParam("surname", "Murray")
                .accept(MediaType.APPLICATION_JSON).put(ClientResponse.class);
        assertEquals(200, resp.getStatus());

        String destUserId = "16";
        resp = service.path(baseUserPath)
                .queryParam("id", destUserId).queryParam("name", "Tom").queryParam("surname", "Murray")
                .accept(MediaType.APPLICATION_JSON).put(ClientResponse.class);
        assertEquals(200, resp.getStatus());

        // creating bills
        resp = service.path(cacheEndpoint)
                .queryParam("id", sourceUserId).queryParam("cacheAmount", "10")
                .accept(MediaType.APPLICATION_JSON).put(ClientResponse.class);
        assertEquals(200, resp.getStatus());

        resp = service.path(cacheEndpoint)
                .queryParam("id", destUserId).queryParam("cacheAmount", "0.5")
                .accept(MediaType.APPLICATION_JSON).put(ClientResponse.class);
        assertEquals(200, resp.getStatus());


        resp = service.path("revolut/bill/transfer")
                .queryParam("sourceId", sourceUserId).queryParam("destId", destUserId).queryParam("cacheAmount", "4.25")
                .accept(MediaType.APPLICATION_JSON).post(ClientResponse.class);

        assertEquals(200, resp.getStatus());

        resp = service.path(cacheEndpoint)
                .queryParam("id", destUserId).accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);

        String response = IOUtils.toString(resp.getEntityInputStream());
        assertEquals("{\"userId\":\"16\",\"money\":4.75}", response);

        resp = service.path(cacheEndpoint)
                .queryParam("id", sourceUserId).accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);

        response = IOUtils.toString(resp.getEntityInputStream());
        assertEquals("{\"userId\":\"15\",\"money\":5.75}", response);
    }
}
