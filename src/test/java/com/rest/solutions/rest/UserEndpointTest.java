package com.rest.solutions.rest;


import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import org.junit.Test;

import javax.ws.rs.core.MediaType;
import java.io.IOException;

import static junit.framework.Assert.assertEquals;

public class UserEndpointTest extends AbstractRestEndpointTest {

    private String basePath = "revolut/user";

    @Test
    public void testUserNotExist() throws IOException {
        Client client = Client.create(new DefaultClientConfig());
        WebResource service = client.resource(getBaseURI());

        ClientResponse resp = service.path(basePath)
                .queryParam("id", "21")
                .accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);

        assertEquals(404, resp.getStatus());
    }

    @Test
    public void testCreateBillCheckExists() throws IOException {
        Client client = Client.create(new DefaultClientConfig());
        WebResource service = client.resource(getBaseURI());

        ClientResponse resp = service.path(basePath)
                .queryParam("id", "12").queryParam("name", "Bill").queryParam("surname", "White")
                .accept(MediaType.APPLICATION_JSON).put(ClientResponse.class);

        assertEquals(200, resp.getStatus());

        resp = service.path(basePath)
                .queryParam("id", "12")
                .accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);

        assertEquals(200, resp.getStatus());
    }

    @Test
    public void testDeleteUser() throws IOException {
        Client client = Client.create(new DefaultClientConfig());
        WebResource service = client.resource(getBaseURI());

        String userId = "13";
        ClientResponse resp = service.path(basePath)
                .queryParam("id", userId).queryParam("name", "Tom").queryParam("surname", "Murray")
                .accept(MediaType.APPLICATION_JSON).put(ClientResponse.class);

        assertEquals(200, resp.getStatus());

        resp = service.path(basePath).queryParam("id", userId)
                .accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
        assertEquals(200, resp.getStatus());

        // deleting user
        resp = service.path(basePath).queryParam("id", userId)
                .accept(MediaType.APPLICATION_JSON).delete(ClientResponse.class);
        assertEquals(200, resp.getStatus());

        // checking user not exist
        resp = service.path(basePath)
                .queryParam("id", userId).accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);

        assertEquals(404, resp.getStatus());
    }
}
