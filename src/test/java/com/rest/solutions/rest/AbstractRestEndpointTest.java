package com.rest.solutions.rest;


import com.google.inject.Injector;
import com.rest.solutions.EntryPoint;
import com.sun.jersey.api.container.grizzly2.GrizzlyServerFactory;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.api.core.ResourceConfig;
import com.sun.jersey.core.spi.component.ioc.IoCComponentProviderFactory;
import com.sun.jersey.guice.spi.container.GuiceComponentProviderFactory;
import org.glassfish.grizzly.http.server.HttpServer;
import org.junit.After;
import org.junit.Before;

import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.net.URI;

public class AbstractRestEndpointTest {

    static final URI BASE_URI = getBaseURI();
    HttpServer server;

    static URI getBaseURI() {
        return UriBuilder.fromUri("http://localhost/").port(9998).build();
    }

    // TODO: change this ugly construction
    class EntryPointTest extends EntryPoint {
        @Override
        protected Injector getInjector() {
            return super.getInjector();
        }
    }

    @Before
    public void startServer() throws IOException {
        Injector injector = new EntryPointTest().getInjector();
        ResourceConfig rc = new PackagesResourceConfig("com.rest.solutions");
        IoCComponentProviderFactory ioc = new GuiceComponentProviderFactory(rc, injector);

        server = GrizzlyServerFactory.createHttpServer(BASE_URI + "revolut/", rc, ioc);
    }

    @After
    public void stopServer() {
        server.stop();
    }
}
